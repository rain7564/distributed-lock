package cn.sprinkle.study.distributedlock.common.lock.thread;

import cn.sprinkle.study.distributedlock.domain.Person;
import cn.sprinkle.study.distributedlock.service.DistributionService;
import org.redisson.api.RedissonClient;

import java.util.concurrent.CountDownLatch;

public class Worker implements Runnable {

    private final CountDownLatch startSignal;
    private final CountDownLatch doneSignal;
    private final DistributionService service;
    private RedissonClient redissonClient;

    public Worker(CountDownLatch startSignal, CountDownLatch doneSignal, DistributionService service, RedissonClient redissonClient) {
        this.startSignal = startSignal;
        this.doneSignal = doneSignal;
        this.service = service;
        this.redissonClient = redissonClient;
    }

    @Override
    public void run() {

        try {
            System.out.println(Thread.currentThread().getName() + " start");

            startSignal.await();

            Integer count = service.aspect(new Person(1, "张三"));
//            Integer count = service.aspect("lock");

            System.out.println(Thread.currentThread().getName() + ": count = " + count);

            doneSignal.countDown();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
