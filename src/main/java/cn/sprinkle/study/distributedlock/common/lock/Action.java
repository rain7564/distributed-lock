package cn.sprinkle.study.distributedlock.common.lock;

@FunctionalInterface
public interface Action {
    void action();
}
