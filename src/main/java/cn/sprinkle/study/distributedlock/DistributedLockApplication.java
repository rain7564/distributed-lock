package cn.sprinkle.study.distributedlock;

import cn.sprinkle.study.distributedlock.common.lock.DistributedLockTemplate;
import cn.sprinkle.study.distributedlock.common.lock.SingleDistributedLockTemplate;
import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.Resource;

import java.io.IOException;

@SpringBootApplication
public class DistributedLockApplication {

	public static void main(String[] args) {
		SpringApplication.run(DistributedLockApplication.class, args);
	}

	@Value("classpath:/redisson-conf.yml")
	Resource configFile;

	@Bean(destroyMethod = "shutdown")
	RedissonClient redisson()
			throws IOException {
		Config config = Config.fromYAML(configFile.getInputStream());
		return Redisson.create(config);
	}

	@Bean
	DistributedLockTemplate distributedLockTemplate(RedissonClient redissonClient) {
		return new SingleDistributedLockTemplate(redissonClient);
	}
}
