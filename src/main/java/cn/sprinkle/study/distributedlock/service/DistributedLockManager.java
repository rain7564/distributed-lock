package cn.sprinkle.study.distributedlock.service;

import cn.sprinkle.study.distributedlock.common.annotation.DistributedLock;
import cn.sprinkle.study.distributedlock.common.lock.Action;
import cn.sprinkle.study.distributedlock.common.lock.thread.Worker1;
import org.springframework.stereotype.Component;

import java.util.function.Supplier;

@Component
public class DistributedLockManager {

    @DistributedLock(argNum = 1, lockNamePost = ".lock")
    public Integer aspect(String lockName, Worker1 worker1) {
        return worker1.aspectBusiness(lockName);
    }

    @DistributedLock(lockName = "lock", lockNamePost = ".lock")
    public int aspect(Supplier<Integer> supplier) {
        return supplier.get();
    }

    @DistributedLock(lockName = "lock", lockNamePost = ".lock")
    public void doSomething(Action action) {
        action.action();
    }
}
